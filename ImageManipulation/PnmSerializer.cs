﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManipulation
{
    public class PnmSerializer : IImageSerializer
    {
        public Image Parse(string imageData)
        {
            //Creating an array to store the information about the image for easier data manipulation
            string[] imgData = imageData.Split(new[] { System.Environment.NewLine }, StringSplitOptions.None);
            //Saving each different type of value to create an Image later
            string metaData = "";
            int maxRange = 0;
            int width = 0;
            int height = 0;
            //Parsing the metadata, the maxrange and the width and height first
            int stringArrayCounter = 1;
            bool foundAllMetadata = false;
            int numberOfCommentLines = 0;
            //If there is no metadata
            if (imgData[stringArrayCounter].Substring(0, 1) != "#")
            {
                maxRange = Int32.Parse(imgData[2]);
                width = Int32.Parse(imgData[1].Substring(0, imgData[1].IndexOf(' ')));
                height = Int32.Parse(imgData[1].Substring(imgData[1].IndexOf(' ') + 1));
                stringArrayCounter += 2;
            }
            //If there is metadata
            else
            {
                while (foundAllMetadata == false)
                {
                    metaData = imgData[stringArrayCounter];
                    numberOfCommentLines++;
                    //Check if next line also contains a comment
                    if (imgData[stringArrayCounter + 1].Substring(0, 1) == "#")
                    {
                        metaData += System.Environment.NewLine;
                        stringArrayCounter++;
                    }
                    else
                    {
                        foundAllMetadata = true;
                        maxRange = Int32.Parse(imgData[stringArrayCounter + 2]);
                        width = Int32.Parse(imgData[stringArrayCounter + 1].Substring(0, imgData[stringArrayCounter + 1].IndexOf(' ')));
                        height = Int32.Parse(imgData[stringArrayCounter + 1].Substring(imgData[stringArrayCounter + 1].IndexOf(' ') + 1));
                        stringArrayCounter += 3;
                    }
                }
            }
            //Checking if the image format is appropriate
            if ((imgData.Length - 4 - numberOfCommentLines) % 3 != 0)
            {
                throw new InvalidDataException("Image passed was not of a valid pnm format");
            }
            Pixel[,] imgPixels = new Pixel[height, width];

            //Fill the pixel array with the values in the imageData string
            int red;
            int green;
            int blue;
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    //Throw exception if the value at the current index of the string goes over the max range limit
                    if (Int32.Parse(imgData[stringArrayCounter]) > maxRange || Int32.Parse(imgData[stringArrayCounter+1]) > maxRange || Int32.Parse(imgData[stringArrayCounter+2]) > maxRange)
                    {
                        throw new InvalidDataException("One of the RGB values of the current pixel is over the limit");
                    }
                    //Create a Pixel with the appropriate information and insert it into the Pixel array
                    red = Int32.Parse(imgData[stringArrayCounter]);
                    green = Int32.Parse(imgData[stringArrayCounter+1]);
                    blue = Int32.Parse(imgData[stringArrayCounter+2]);
                    Pixel pixel = new Pixel(red, green, blue);
                    imgPixels[i, j] = pixel;
                    stringArrayCounter += 3;
                }
            }
            Image image = new Image(metaData, maxRange, imgPixels);
            return image;
        }

        public string Serialize(Image i)
        {
            string imageData = "P3" + System.Environment.NewLine;
            //Adding metadata (if there is any)
            if (i.Metadata != String.Empty)
            {
                imageData += i.Metadata + System.Environment.NewLine;
            }
            //Adding width and height info
            imageData += i.GetLength(1) + " " + i.GetLength(0) + System.Environment.NewLine;
            //Adding maxRange info
            imageData += i.MaxRange + System.Environment.NewLine;
            for (int index = 0; index < i.GetLength(0); index++)
            {
                //Add each pixel color value to its own line
                for (int jindex = 0; jindex < i.GetLength(1); jindex++)
                {
                    imageData += i[index, jindex].Red + System.Environment.NewLine;
                    imageData += i[index, jindex].Green + System.Environment.NewLine;
                    imageData += i[index, jindex].Blue + System.Environment.NewLine;
                }
            }
            return imageData;
        }
    }
}
