﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManipulation
{
    public class PgmSerializer : IImageSerializer
    {
        public string Serialize(Image i)
        {
            string imageData="P2" + System.Environment.NewLine;
            //Adding metadata (if there is any)
            if (i.Metadata != String.Empty)
            {
                imageData += i.Metadata + System.Environment.NewLine;
            }
            //Adding width and height info
            imageData += i.GetLength(1) + " " + i.GetLength(0) + System.Environment.NewLine;
            //Adding maxRange info
            imageData += i.MaxRange + System.Environment.NewLine;
            for(int index=0; index<i.GetLength(0); index++)
            {
                //Add the color value for red of each pixel on each line since greyscale images rgb values have the same value
                for(int jindex=0; jindex<i.GetLength(1); jindex++)
                {
                    imageData += i[index, jindex].Red + System.Environment.NewLine;
                }
            }
            return imageData;
        }

        public Image Parse(string imageData)
        {
            //Creating an array to store the information about the image for easier data manipulation
            string[] imgData = imageData.Split(new[] { System.Environment.NewLine }, StringSplitOptions.None);

            //Saving each different type of value to create an Image later
            string metaData = "";
            int maxRange = 0;
            int width = 0;
            int height = 0;
            //Parsing the metadata, the maxrange and the width and height first
            int stringArrayCounter = 1;
            bool foundAllMetadata = false;
            //If there is no metadata
            if (imgData[stringArrayCounter].Substring(0, 1) != "#")
            {
                maxRange = Int32.Parse(imgData[2]);
                width = Int32.Parse(imgData[1].Substring(0, imgData[1].IndexOf(' ')));
                height = Int32.Parse(imgData[1].Substring(imgData[1].IndexOf(' ') + 1));
                stringArrayCounter += 2;
            }
            //If there is metadata
            else
            {
                while (foundAllMetadata == false)
                {
                    metaData = imgData[stringArrayCounter];
                    //Check if next line also contains a comment
                    if(imgData[stringArrayCounter+1].Substring(0, 1) == "#")
                    {
                        metaData += System.Environment.NewLine;
                        stringArrayCounter++;
                    }
                    else
                    {
                        foundAllMetadata = true;
                        maxRange = Int32.Parse(imgData[stringArrayCounter + 2]);
                        width = Int32.Parse(imgData[stringArrayCounter+1].Substring(0, imgData[stringArrayCounter + 1].IndexOf(' ')));
                        height = Int32.Parse(imgData[stringArrayCounter + 1].Substring(imgData[stringArrayCounter + 1].IndexOf(' ') + 1));
                        stringArrayCounter += 3;
                    }
                }
            }
            Pixel[,] imgPixels = new Pixel[height, width];

            //Fill the pixel array with the values in the imageData string
            int colour;
            for(int i=0; i<height; i++)
            {
                for(int j=0; j<width; j++, stringArrayCounter++)
                {
                    //Throw exception if the value at the current index of the string goes over the max range limit
                    if(Int32.Parse(imgData[stringArrayCounter]) > maxRange)
                    {
                        throw new InvalidDataException("Current colour value is over the limit");
                    }
                    //Create a Pixel with the appropriate information and insert it into the Pixel array
                    colour = Int32.Parse(imgData[stringArrayCounter]);
                    Pixel pixel = new Pixel(colour);
                    imgPixels[i, j] = pixel;
                }
            }
            Image image = new Image(metaData, maxRange, imgPixels);
            return image;
        }

    }
}
