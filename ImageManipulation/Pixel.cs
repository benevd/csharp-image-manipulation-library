﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManipulation
{
    public class Pixel
    {
        private int red;
        private int green;
        private int blue;

        public int Red
        {
            get
            {
                return red;
            }
            private set
            {
                red = value;
            }
        }

        public int Green
        {
            get
            {
                return green;
            }
            private set
            {
                green = value;
            }
        }

        public int Blue
        {
            get
            {
                return blue;
            }
            private set
            {
                blue = value;
            }
        }

        public Pixel(int r, int g, int b)
        {
            if (r > 255 || r < 0 || g > 255 || g < 0 || b > 255 || b < 0)
            {
                throw new ArgumentException("One of the intensity values is outside of the allowed range (0-255)");
            }
            Red = r;
            Green = g;
            Blue = b;
        }

        public Pixel(int intensity) : this(intensity, intensity, intensity)
        {
            
        }

        public int Grey()
        {
            int average;
            average = (Red + Green + Blue) / 3;
            return average;
        }
    }
}
