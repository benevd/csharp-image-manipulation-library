﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManipulation
{
    public class Image
    {
        private string metadata;
        private int maxRange;
        private Pixel[,] data;

        public string Metadata
        {
            get
            {
                return metadata;
            }
            private set
            {
                metadata = value;
            }
        }

        public int MaxRange
        {
            get
            {
                return maxRange;
            }
            private set
            {
                maxRange = value;
            }
        }

        //Indexer
        public Pixel this[int row, int col]
        {
            get
            {
                return data[row, col];
            }
            private set
            {
                data[row, col] = value;
            }
        }

        //Constructor
        public Image(string metadata, int maxRange, Pixel[,] data)
        {
            if(maxRange < 0)
            {
                throw new ArgumentException("Max range cannot be negative");
            }
           Metadata = metadata;
           MaxRange = maxRange;
           Pixel[,] copy = data.Clone() as Pixel[,];
           this.data = copy;
        }

        //GetLenght: get the width and height of an image using its data array
        public int GetLength(int dimension)
        {
            if(dimension == 0 || dimension == 1)
            {
                return data.GetLength(dimension);
            }
            else
            {
                throw new ArgumentException("Dimension is invalid");
            }
        }

        //ToGrey: converts the Image to greyscale
        public void ToGrey()
        {
            //Creating a new data variable that will contain the pixels of the grey version of the current image
            Pixel[,] greyscaleData = new Pixel[GetLength(0), GetLength(1)];
            //Going through each pixel of the image and changing its colour to greyscale
            for(int i=0; i< GetLength(0); i++)
            {
                for(int j=0; j< GetLength(1); j++)
                {
                    int grey = data[i, j].Grey();
                    greyscaleData[i, j] = new Pixel(grey);
                }
            }
            //Replacing the color version of the image with the grey one
            data = greyscaleData;
        }

        //Flip: flips the image either horizontally or vertically
        public void Flip(bool horizontal)
        {
            //Creating variables that store the Image width and height (zero-indexed)
            int h = GetLength(0)-1;
            int w = GetLength(1)-1;
            //Creating an image of the same size as the original that will become the flipped version
            Pixel[,] flipped = new Pixel[GetLength(0), GetLength(1)];
            //Flip horizontally(across the y axis)
            if (horizontal)
            {
                //Going through each pixel of the original image and modifying copy to become flipped
                for(int i=0; i<h; i++)
                {
                    for(int j=0; j<w; j++)
                    {
                        flipped[i, j] = data[i, w-j];
                    }
                }
            }
            //Flip vertically(across the x axis)
            else
            {
                //Going through each pixel of the original image and modifying copy to become flipped
                for (int i = 0; i < h; i++)
                {
                    for (int j = 0; j < w; j++)
                    {
                        flipped[i, j] = data[h - i, j];
                    }
                }
            }

            //Replacing the original image with the flipped image
            data = flipped;
        }

        //Crop: crops a rectangular section of the image
        public void Crop(int startX, int startY, int endX, int endY)
        {
            //Creating image height and width variables and using them to verify input values
            int width = GetLength(1);
            int height = GetLength(0);
            if(startX < 0 || startY <0 || endX >= width || endY >= height)
            {
                throw new ArgumentException("Invalid input");
            }
            //Cropping the image
            Pixel[,] newImage = new Pixel[endY - startY, endX - startX];
            for (int i = startY; i <= endY; i++)
            {
                for(int j=startX; j <= endX; j++)
                {
                    newImage[i, j] = data[i, j];
                }
            }
            //Assigning the cropped image to the original's variable
            data = newImage;
        }
    }
}
