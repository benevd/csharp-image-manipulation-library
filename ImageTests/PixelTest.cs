﻿using ImageManipulation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageTests
{
    [TestClass]
    public class PixelTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestConstructor()
        {
                Pixel pixel = new Pixel(40, 256, 90);
        }

        [TestMethod]
        public void TestOverloadedConstructor()
        {
            Pixel pixel = new Pixel(60);
            Pixel pixel2 = new Pixel(60, 60, 60);

            Assert.AreEqual(pixel2.Blue, pixel.Red);
        }

        [TestMethod]
        public void TestGrey()
        {
            Pixel pixel = new Pixel(60, 40, 90);
            Pixel pixel2 = new Pixel(63);

            int intensity = pixel.Grey();

            Assert.AreEqual(pixel2.Green, intensity);
        }
    }
}
