﻿using ImageManipulation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageTests
{
    [TestClass]
    public class PgmSerializerTest
    {
        [TestMethod]
        public void TestSerialize() {
            string metadata = "#Test Image by Dimitar Benev";
            int maxrange = 255;
            Pixel lightGrey = new Pixel(200);
            Pixel white = new Pixel(255);
            Pixel black = new Pixel(0);
            Pixel darkGrey = new Pixel(50);
            Pixel[,] pixels = new Pixel[2, 3];

            pixels[0, 0] = white;
            pixels[0, 1] = black;
            pixels[0, 2] = white;
            pixels[1, 0] = white;
            pixels[1, 1] = darkGrey;
            pixels[1, 2] = lightGrey;

            Image imageToBeSerialized = new Image(metadata, maxrange, pixels);
            PgmSerializer greyscaleSerializer = new PgmSerializer();
            string serializedResult = greyscaleSerializer.Serialize(imageToBeSerialized);
            string expected = "P2" + System.Environment.NewLine;
            expected += metadata + System.Environment.NewLine;
            expected += imageToBeSerialized.GetLength(1) + " " + imageToBeSerialized.GetLength(0) + System.Environment.NewLine;
            expected += maxrange + System.Environment.NewLine;
            expected += "255" + System.Environment.NewLine + "0" + System.Environment.NewLine + "255" + System.Environment.NewLine + "255" + System.Environment.NewLine + "50" + System.Environment.NewLine + "200";

            StringAssert.Equals(expected, serializedResult);

        }

        [TestMethod]
        [ExpectedException(typeof(InvalidDataException))]
        public void TestParseException()
        {
            PgmSerializer greyscaleSerializer = new PgmSerializer();
            string test = "P2" + System.Environment.NewLine;
            test += "#Comment" + System.Environment.NewLine;
            test +=  "3 2" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine + "0" + System.Environment.NewLine + "256" + System.Environment.NewLine + "255" + System.Environment.NewLine + "50" + System.Environment.NewLine + "200";

            greyscaleSerializer.Parse(test);

        }

        [TestMethod]
        public void TestParseNoMetaData()
        {
            PgmSerializer greyscaleSerializer = new PgmSerializer();
            string test = "P2" + System.Environment.NewLine;
            test += "3 2" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine + "0" + System.Environment.NewLine + "255" + System.Environment.NewLine + "255" + System.Environment.NewLine + "50" + System.Environment.NewLine + "200";

            string metadata = "";
            int maxrange = 255;
            Pixel lightGrey = new Pixel(200);
            Pixel white = new Pixel(255);
            Pixel black = new Pixel(0);
            Pixel darkGrey = new Pixel(50);
            Pixel[,] pixels = new Pixel[2, 3];

            pixels[0, 0] = white;
            pixels[0, 1] = black;
            pixels[0, 2] = white;
            pixels[1, 0] = white;
            pixels[1, 1] = darkGrey;
            pixels[1, 2] = lightGrey;

            Image imageToBeSerialized = new Image(metadata, maxrange, pixels);
            Image parsedThenSerialized = greyscaleSerializer.Parse(test);
            string expected = greyscaleSerializer.Serialize(imageToBeSerialized);
            string result = greyscaleSerializer.Serialize(parsedThenSerialized);

            StringAssert.Equals(expected, result);
        }

        [TestMethod]
        public void TestParseMetaData()
        {
            PgmSerializer greyscaleSerializer = new PgmSerializer();
            string test = "P2" + System.Environment.NewLine;
            test += "#Test image" + System.Environment.NewLine;
            test += "#This is a greyscale image" + System.Environment.NewLine;
            test += "3 2" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine + "0" + System.Environment.NewLine + "255" + System.Environment.NewLine + "255" + System.Environment.NewLine + "50" + System.Environment.NewLine + "200";

            string metadata = "#Test image" + System.Environment.NewLine + "#This is a greyscale image";
            int maxrange = 255;
            Pixel lightGrey = new Pixel(200);
            Pixel white = new Pixel(255);
            Pixel black = new Pixel(0);
            Pixel darkGrey = new Pixel(50);
            Pixel[,] pixels = new Pixel[2, 3];

            pixels[0, 0] = white;
            pixels[0, 1] = black;
            pixels[0, 2] = white;
            pixels[1, 0] = white;
            pixels[1, 1] = darkGrey;
            pixels[1, 2] = lightGrey;

            Image imageToBeSerialized = new Image(metadata, maxrange, pixels);
            Image parsedThenSerialized = greyscaleSerializer.Parse(test);
            string expected = greyscaleSerializer.Serialize(imageToBeSerialized);
            string result = greyscaleSerializer.Serialize(parsedThenSerialized);

            StringAssert.Equals(expected, result);
        }
    }
}
