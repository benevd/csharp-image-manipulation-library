﻿using ImageManipulation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageTests
{
    [TestClass]
    public class PnmSerializerTest
    {
        [TestMethod]
        public void TestSerialize()
        {
            string metadata = "#Test Image by Dimitar Benev";
            int maxrange = 255;
            Pixel red = new Pixel(255, 0, 0);
            Pixel green = new Pixel(0, 255, 0);
            Pixel blue = new Pixel(0, 0, 255);
            Pixel aqua = new Pixel(25, 200, 250);
            Pixel pink = new Pixel(255, 0, 255);
            Pixel yellow = new Pixel(255, 255, 0);
            Pixel[,] pixels = new Pixel[2, 3];

            pixels[0, 0] = red;
            pixels[0, 1] = green;
            pixels[0, 2] = aqua;
            pixels[1, 0] = pink;
            pixels[1, 1] = yellow;
            pixels[1, 2] = blue;

            Image imageToBeSerialized = new Image(metadata, maxrange, pixels);
            PnmSerializer colorSerializer = new PnmSerializer();

            string serializedResult = colorSerializer.Serialize(imageToBeSerialized);
            string expected = "P3" + System.Environment.NewLine;
            expected += metadata + System.Environment.NewLine;
            expected += imageToBeSerialized.GetLength(1) + " " + imageToBeSerialized.GetLength(0) + System.Environment.NewLine;
            expected += maxrange + System.Environment.NewLine;
            expected += "255" + System.Environment.NewLine + "0" + System.Environment.NewLine + "0" + System.Environment.NewLine;
            expected += "0" + System.Environment.NewLine + "255" + System.Environment.NewLine + "0" + System.Environment.NewLine;
            expected += "25" + System.Environment.NewLine + "200" + System.Environment.NewLine + "250" + System.Environment.NewLine;
            expected += "255" + System.Environment.NewLine + "0" + System.Environment.NewLine + "255" + System.Environment.NewLine;
            expected += "255" + System.Environment.NewLine + "255" + System.Environment.NewLine + "0" + System.Environment.NewLine;
            expected += "0" + System.Environment.NewLine + "0" + System.Environment.NewLine + "255" + System.Environment.NewLine;

            StringAssert.Equals(expected, serializedResult);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidDataException))]
        public void TestParseException()
        {
            PnmSerializer colorSerializer = new PnmSerializer();

            string test = "P3" + System.Environment.NewLine;
            test += "#Comment" + System.Environment.NewLine;
            test += "3 2" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine + "0" + System.Environment.NewLine + "0" + System.Environment.NewLine;
            test += "0" + System.Environment.NewLine + "255" + System.Environment.NewLine + "0" + System.Environment.NewLine;
            test += "25" + System.Environment.NewLine + "200" + System.Environment.NewLine + "250" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine + "0" + System.Environment.NewLine + "255" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine + "255" + System.Environment.NewLine + "0" + System.Environment.NewLine;
            test += "0" + System.Environment.NewLine + "0" + System.Environment.NewLine;

            colorSerializer.Parse(test);

        }

        [TestMethod]
        public void TestParseNoMetadata()
        {
            PnmSerializer colorSerializer = new PnmSerializer();

            string test = "P3" + System.Environment.NewLine;
            test += "3 2" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine + "0" + System.Environment.NewLine + "0" + System.Environment.NewLine;
            test += "0" + System.Environment.NewLine + "255" + System.Environment.NewLine + "0" + System.Environment.NewLine;
            test += "25" + System.Environment.NewLine + "200" + System.Environment.NewLine + "250" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine + "0" + System.Environment.NewLine + "255" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine + "255" + System.Environment.NewLine + "0" + System.Environment.NewLine;
            test += "0" + System.Environment.NewLine + "0" + System.Environment.NewLine + "255" + System.Environment.NewLine;

            string metadata = "";
            int maxrange = 255;
            Pixel red = new Pixel(255, 0, 0);
            Pixel green = new Pixel(0, 255, 0);
            Pixel blue = new Pixel(0, 0, 255);
            Pixel aqua = new Pixel(25, 200, 250);
            Pixel pink = new Pixel(255, 0, 255);
            Pixel yellow = new Pixel(255, 255, 0);
            Pixel[,] pixels = new Pixel[2, 3];

            pixels[0, 0] = red;
            pixels[0, 1] = green;
            pixels[0, 2] = aqua;
            pixels[1, 0] = pink;
            pixels[1, 1] = yellow;
            pixels[1, 2] = blue;

            Image imageToBeSerialized = new Image(metadata, maxrange, pixels);
            Image parsedThenSerialized = colorSerializer.Parse(test);
            string expected = colorSerializer.Serialize(imageToBeSerialized);
            string result = colorSerializer.Serialize(parsedThenSerialized);

            StringAssert.Equals(expected, result);
        }

        [TestMethod]
        public void TestParseMetadata()
        {
            PnmSerializer colorSerializer = new PnmSerializer();

            string test = "P3" + System.Environment.NewLine;
            test += "#Test image" + System.Environment.NewLine;
            test += "#This is a color image" + System.Environment.NewLine;
            test += "#Made by Dimitar Benev" + System.Environment.NewLine;
            test += "3 2" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine + "0" + System.Environment.NewLine + "0" + System.Environment.NewLine;
            test += "0" + System.Environment.NewLine + "255" + System.Environment.NewLine + "0" + System.Environment.NewLine;
            test += "25" + System.Environment.NewLine + "200" + System.Environment.NewLine + "250" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine + "0" + System.Environment.NewLine + "255" + System.Environment.NewLine;
            test += "255" + System.Environment.NewLine + "255" + System.Environment.NewLine + "0" + System.Environment.NewLine;
            test += "0" + System.Environment.NewLine + "0" + System.Environment.NewLine + "255" + System.Environment.NewLine;

            string metadata = "#Test image" + System.Environment.NewLine + "#This is a color image" + System.Environment.NewLine + "#Made by Dimitar Benev";
            int maxrange = 255;
            Pixel red = new Pixel(255, 0, 0);
            Pixel green = new Pixel(0, 255, 0);
            Pixel blue = new Pixel(0, 0, 255);
            Pixel aqua = new Pixel(25, 200, 250);
            Pixel pink = new Pixel(255, 0, 255);
            Pixel yellow = new Pixel(255, 255, 0);
            Pixel[,] pixels = new Pixel[2, 3];

            pixels[0, 0] = red;
            pixels[0, 1] = green;
            pixels[0, 2] = aqua;
            pixels[1, 0] = pink;
            pixels[1, 1] = yellow;
            pixels[1, 2] = blue;

            Image imageToBeSerialized = new Image(metadata, maxrange, pixels);
            Image parsedThenSerialized = colorSerializer.Parse(test);
            string expected = colorSerializer.Serialize(imageToBeSerialized);
            string result = colorSerializer.Serialize(parsedThenSerialized);

            StringAssert.Equals(expected, result);
        }
    }
}
