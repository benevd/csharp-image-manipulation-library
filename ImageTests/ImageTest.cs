﻿using ImageManipulation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageTests
{
    [TestClass]
    public class ImageTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestConstructor()
        {
            string metadata = "Test image";
            int maxrange = -1;
            Pixel red = new Pixel(255, 0, 0);
            Pixel[,] pixels = new Pixel[1, 1];

            pixels[0, 0] = red;

            Image onebit = new Image(metadata, maxrange, pixels);
        }

        [TestMethod]
        public void TestGetLength()
        {
            string metadata = "Test image";
            int maxrange = 255;
            Pixel red = new Pixel(255, 0, 0);
            Pixel green = new Pixel(0, 255, 0);
            Pixel blue = new Pixel(0, 0, 255);
            Pixel[,] pixels = new Pixel[1, 3];

            pixels[0, 0] = red;
            pixels[0, 1] = blue;
            pixels[0, 2] = green;

            Image rgb = new Image(metadata, maxrange, pixels);

            int width = rgb.GetLength(1);

            Assert.AreEqual(3, width);
        }

        [TestMethod]
        public void TestToGrey()
        {
            string metadata = "Test image";
            int maxrange = 255;
            Pixel brown = new Pixel(100, 50, 0);
            Pixel green = new Pixel(0, 255, 0);
            Pixel lightBlue = new Pixel(0, 100, 200);
            Pixel[,] pixels = new Pixel[1, 3];

            pixels[0, 0] = brown;
            pixels[0, 1] = lightBlue;
            pixels[0, 2] = green;

            Image imageToBeConvertedToGrey = new Image(metadata, maxrange, pixels);

            imageToBeConvertedToGrey.ToGrey();

            Assert.AreEqual(100, imageToBeConvertedToGrey[0, 1].Red);
        }

        [TestMethod]
        public void TestHorizontalFlip()
        {
            string metadata = "Test image";
            int maxrange = 255;
            Pixel red = new Pixel(255, 0, 0);
            Pixel brown = new Pixel(100, 50, 0);
            Pixel green = new Pixel(0, 255, 0);
            Pixel lightBlue = new Pixel(0, 100, 200);
            Pixel[,] pixels = new Pixel[4, 5];

            pixels[0, 0] = brown;
            pixels[0, 1] = lightBlue;
            pixels[0, 2] = green;
            pixels[0, 3] = brown;
            pixels[0, 4] = lightBlue;
            pixels[1, 0] = green;
            pixels[1, 1] = green;
            pixels[1, 2] = brown;
            pixels[1, 3] = green;
            pixels[1, 4] = lightBlue;
            pixels[2, 0] = brown;
            pixels[2, 1] = red;
            pixels[2, 2] = brown;
            pixels[2, 3] = brown;
            pixels[2, 4] = brown;
            pixels[3, 0] = lightBlue;
            pixels[3, 1] = lightBlue;
            pixels[3, 2] = lightBlue;
            pixels[3, 3] = lightBlue;
            pixels[3, 4] = green;

            Image imageToBeHorizontallyFlipped = new Image(metadata, maxrange, pixels);

            imageToBeHorizontallyFlipped.Flip(true);

            Assert.AreEqual(255, imageToBeHorizontallyFlipped[2, 3].Red);
        }

        [TestMethod]
        public void TestVerticalFlip()
        {
            string metadata = "Test image";
            int maxrange = 255;
            Pixel red = new Pixel(255, 0, 0);
            Pixel brown = new Pixel(100, 50, 0);
            Pixel green = new Pixel(0, 255, 0);
            Pixel lightBlue = new Pixel(0, 100, 200);
            Pixel[,] pixels = new Pixel[4, 5];

            pixels[0, 0] = brown;
            pixels[0, 1] = lightBlue;
            pixels[0, 2] = green;
            pixels[0, 3] = brown;
            pixels[0, 4] = lightBlue;
            pixels[1, 0] = green;
            pixels[1, 1] = green;
            pixels[1, 2] = brown;
            pixels[1, 3] = green;
            pixels[1, 4] = lightBlue;
            pixels[2, 0] = brown;
            pixels[2, 1] = red;
            pixels[2, 2] = brown;
            pixels[2, 3] = brown;
            pixels[2, 4] = brown;
            pixels[3, 0] = lightBlue;
            pixels[3, 1] = lightBlue;
            pixels[3, 2] = lightBlue;
            pixels[3, 3] = lightBlue;
            pixels[3, 4] = green;

            Image imageToBeHorizontallyFlipped = new Image(metadata, maxrange, pixels);

            imageToBeHorizontallyFlipped.Flip(false);

            Assert.AreEqual(255, imageToBeHorizontallyFlipped[1, 1].Red);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestCropException()
        {
            string metadata = "Test image";
            int maxrange = 255;
            Pixel red = new Pixel(255, 0, 0);
            Pixel green = new Pixel(0, 255, 0);
            Pixel blue = new Pixel(0, 0, 255);
            Pixel[,] pixels = new Pixel[1, 3];

            pixels[0, 0] = red;
            pixels[0, 1] = blue;
            pixels[0, 2] = green;

            Image imageToBeCropped = new Image(metadata, maxrange, pixels);

            imageToBeCropped.Crop(0, 0, 4, 2);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void TestCrop()
        {
            string metadata = "Test image";
            int maxrange = 255;
            Pixel red = new Pixel(255, 0, 0);
            Pixel brown = new Pixel(100, 50, 0);
            Pixel green = new Pixel(0, 255, 0);
            Pixel lightBlue = new Pixel(0, 100, 200);
            Pixel[,] pixels = new Pixel[4, 5];

            pixels[0, 0] = brown;
            pixels[0, 1] = lightBlue;
            pixels[0, 2] = green;
            pixels[0, 3] = brown;
            pixels[0, 4] = lightBlue;
            pixels[1, 0] = green;
            pixels[1, 1] = green;
            pixels[1, 2] = brown;
            pixels[1, 3] = green;
            pixels[1, 4] = lightBlue;
            pixels[2, 0] = brown;
            pixels[2, 1] = red;
            pixels[2, 2] = brown;
            pixels[2, 3] = brown;
            pixels[2, 4] = brown;
            pixels[3, 0] = lightBlue;
            pixels[3, 1] = lightBlue;
            pixels[3, 2] = lightBlue;
            pixels[3, 3] = lightBlue;
            pixels[3, 4] = green;

            Image imageToBeCropped = new Image(metadata, maxrange, pixels);

            imageToBeCropped.Crop(0, 0, 3, 2);

            Pixel accessAttempt = imageToBeCropped[3, 4];
        }
    }
}
